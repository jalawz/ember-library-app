import Controller from '@ember/controller';
import { gte, match, and } from '@ember/object/computed';

export default Controller.extend({
    emailAddress: '',
    message: '',

    isValidEmail: match('emailAddress', /^.+@.+\..+$/),
    isLongEnough: gte('message.length', 5),
    isDisabled: and('isValidEmail', 'isLongEnough'),

    actions: {
        sendMessage() {
            alert(`${this.get('emailAddress')} message sent`);
            this.set('emailAddress', '');
            this.set('message', '');
            this.set('responseMessage', 'We got your message and we’ll get in touch soon');
        }
    }
});
